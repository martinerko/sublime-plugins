var fs = require("fs");
var os = require("os");
var filenameToParse = process.argv[2];

var data = fs.readFileSync(filenameToParse);
data = "//last update: " + new Date() + os.EOL + data;
fs.writeFileSync(filenameToParse, data);
console.log("File %s was updated", filenameToParse);
return;