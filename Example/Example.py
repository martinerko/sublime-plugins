import sublime, sublime_plugin, subprocess
 
class ExampleCommand(sublime_plugin.TextCommand):
    #to be able to reload edited file after making changes with external script, we need to call revert method
    #by using run method only, editing file will be reloaded after loosing focus
    #by using run_ method we can reload edited file directly
    def run_(self, edit):
        #self.view.sel().clear()
        p = subprocess.Popen(["node", "app.js", self.view.file_name()], stdout=subprocess.PIPE);
        print p.communicate()[0]
        #reload screen
        sublime.active_window().active_view().run_command("revert")