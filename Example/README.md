# Example Sublime plugin #
This plugin demonstrates how to implement Sublime Text 2 plugin which can call external node.js script to update edited file.

## Installation ##
To install this plugin copy this folder into *Packages* folder located: 

- OS X: ~/Library/Application Support/Sublime Text 2
- Windows: %APPDATA%\Sublime Text 2
- Linux: ~/.config/sublime-text-2

## Usage ##
You can execute this plugin:

- press **Ctrl `** to activate Sublime's command line and then type   **view.run_command('example')**

or
 
- press **Ctrl Alt X**

or

- menu **Tools -> My Extra Tools -> Example Plugin**

## Expected Result ##
This plugin will insert new comment line to the beginning of the edited file such as sample bellow:

*//last update: Tue Dec 23 2014 23:38:08 GMT+0100 (Central Europe Standard Time)*

