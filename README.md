# Sublime plugins #
This repository contains several Sublime 2 plugins.
Each plugin is stored under special folder.

## Prerequisites ##
You must have python  2.7.x and node.js installed on your system.

## Installation ##
To install some of the plugins, just clone this repository and copy choosen plugin/subfolder into *Packages* folder located: 

- OS X: ~/Library/Application Support/Sublime Text 2
- Windows: %APPDATA%\Sublime Text 2
- Linux: ~/.config/sublime-text-2

## Usage ##
For usage details please see README.md under given plugin folder.